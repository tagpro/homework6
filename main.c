#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>

#define L 200

struct node {
    struct node *left;
    struct node *right;
char *name;
};
struct node *creator(char *name) {
   struct node * r= (struct node *)malloc(sizeof(struct node));
    r->name = name;
    return r;
}
void view(struct node* root) {
char str[L];
    static int a = 0;
    int i = a;
    static int prLev = 0, maxLev = 0;

if (root) {
        while (i--) {
           printf("\t");
            if(i == 1) {
                printf("|");
            }
        }
        
        i = a;
        if(a !=0) {
            printf("|\n");
            while(i--) {
                printf("\t|");
            }
            printf("\b");
        }

        mkdir(root->name, 0777);
        chdir(root->name);
	
       getcwd(str, L);
       
        printf("%s %s %s %s\n", "Создан каталог", root->name,"Путь:", str);

        ++a;

        view(root->left);
        view(root->right);

        chdir("../");
 
        --a;
    }
}
int main(void) {
    struct node *stack;

   stack = creator("<Имя потомка>");
   stack->left = creator("<имя мамы>");
   stack->right = creator("<Имя папы>");
   stack->left->left = creator("<Имя бабушки 1>");
   stack->left->right = creator("<Имя деда 1>");
   stack->right->left = creator("<Имя бабушки 2>");
   stack->right->right = creator("<Имя деда 2>");

    view(stack);
    return 0;
}